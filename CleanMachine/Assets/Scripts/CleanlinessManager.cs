﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CleanlinessManager : MonoBehaviour {

	private int counter;
	public GameObject vacuum;
	public float timeLeft;
	public float timeLeftIntial;
	public bool gameActive;
	public Text timerText; 
	public int cleanlinessMax;
	public Slider cleanBar;
	public Text winText;
	public Text loseText;
	public Text startText;
	public float growFactor;
	public Button Retry;
	public Button MoveOn;

	public string nextScene;

	private VacuumMovement vacuumMovement;





	// Use this for initialization
	void Start () {
		counter = 0;
		gameActive = true;
		cleanBar.maxValue = cleanlinessMax;
		StartCoroutine ("LevelStart");
		timeLeft = timeLeftIntial;
		vacuumMovement = vacuum.GetComponent<VacuumMovement> ();

	}
	
	// Update is called once per frame
	void Update () {
		if (gameActive == true) {
			timeLeft -= Time.deltaTime;
			timerText.text = Mathf.Round (timeLeft).ToString ();

			if (timeLeft <= 0) {
				gameActive = false;
				gameOver ();
			}
		}
	}


	public void AddOne(int One)
	{
		counter += One;
		cleanBar.value = counter;
		if (counter >= cleanlinessMax) {
			gameActive = false;
			gameOver ();
		}


	}


	public void gameOver()
	{
		Debug.Log("OVER");
	
		if (counter < cleanlinessMax) {
			loseText.gameObject.SetActive (true);
			Retry.gameObject.SetActive (true);
			Retry.onClick.AddListener(BacktoMenu);
		


		}else if (counter >= cleanlinessMax )
		{
			winText.gameObject.SetActive (true);
			MoveOn.gameObject.SetActive (true);
			MoveOn.onClick.AddListener(NextLevel);
			}
	}

	IEnumerator LevelStart()
	{ 
		startText.gameObject.SetActive (true);
		yield return new WaitForSeconds (1);
		startText.gameObject.SetActive (false);
	}

	public void BacktoMenu()
	{
		SceneManager.LoadScene ("Menu");
	}

	public void NextLevel()
	{
		SceneManager.LoadScene (nextScene);
	}


}