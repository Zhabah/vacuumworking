﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class MenuScript : MonoBehaviour {
	
	public Rigidbody2D vacuum;
	public Button beginGame;

	// Use this for initialization
	void Start () {
		Button button = beginGame.GetComponent<Button>();
		button.onClick.AddListener(TaskOnClick);
		VacuumRoll ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void VacuumRoll()
	{
		vacuum.velocity = new Vector3 (8, 0, 0);
	}

	public void TaskOnClick()
	{
		StartCoroutine ("Begin");
	}

	IEnumerator Begin()
	{yield return new WaitForSeconds (1);
	SceneManager.LoadScene ("LivingLevel");
	}
}
